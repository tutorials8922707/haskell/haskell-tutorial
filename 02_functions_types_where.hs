-- Functions
in_range min max x =
	x >= min && x <= max

in_range 0 5 3

x :: Integer
x = 1

y :: Bool
y = True

z :: Float
z = 3.1415

in_range :: Integer -> Integer -> Integer -> Integer -> Bool
in_range min max x = x >= min && x <= max

in_range 0.5 1.5 1 -- Type erro
in_range 0 5 3 -- Correct

-- let
in_range min max x =
	let in_lower_bound = min <= x
			in_upper_bound = max >= x
	in
	in_lower_bound && in_upper_bound

-- where
in_range min max x = ilb && iub
	where
		ilb = min <= x
		lub = max >= x

-- if
if_range min max x =
	if ilb then iub else False
	where
		ilb = min <= x
		iub = max >= x
