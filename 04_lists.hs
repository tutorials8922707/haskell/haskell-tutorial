import Data.List

-- Genereate ascening order list
asc :: Int -> Int -> [Int]
asc n m
	| m < n = []
	| m == n = [m]
	| m > n = n : asc (n + 1) m

asc 1 3 => [1, 2, 3]

head :: [a] -> a
head [1, 2, 3, 4, 5] => 1

tail :: [a] -> [a]
tail [1, 2, 3, 4, 5] => [2, 3, 4, 5]

length :: [a] -> Int
lenght [1, 2, 3, 4, 5] => 5

init :: [a] -> [a]
init [1, 2, 3, 4, 5] => [1, 2, 3, 4]

null :: [a] -> Bool
null [] => True

null [1, 2, 3] => False

and :: [Bool] -> Bool
and [True, False, True, False] => False

or :: [Bool] -> Bool
or [True, False, True] => True

[ 2*x | x <- [1, 2, 3] ] => [2, 4, 6]
[ 2*x | x <- [1, 2, 3], x > 1 ] => [4, 6]

[ (x, y) | x <- [1, 2, 3], y <- ['a', 'b'] ] => [(1, 'a'), (1, 'b'), (2, 'a'), (2, 'b'), (3, 'a'), (3, 'b')]

sum :: [Int] -> Int
sum [] = 0
sum (x:xs) = x + sum xs

evens :: [Int] -> [Int]
evens [] = []
evens (x:xs)
	| mod x 2 == 0 = x : evens xs
	| otherwise = evens xs

fst :: (a,b) ->
fst (x,_) = x

snd :: (a,b) -> b
snd (_,y) = y

let (x,y) = (1,2) in x => 1

addTupples :: [(Int, Int)] -> [Int]
addTupples xs = [ x + y | (x, y), <- xs ]

addTupples [(1, 2), (2, 3), (100, 100)] => [3, 5, 100]
