felem :: (Eq a) => a -> [a] -> Bool
felem _ [] = False
felem e (x:xs) = (e == x) || (felem e xs)

fnub :: (Eq a) => [a] -> [a]
fnub [] = []
fnub (x:xs)
 | x `felem` xs = fnub xs
 | otherwise = x : fnub xs

fisAsc :: [Int] -> Bool
fisAsc [] = True
fisAsc [a, b] = a <= b
fisAsc (x:xs)
 | x <= head xs = fisAsc xs
 | otherwise = False

fisAsc2 :: [Int] -> Bool
fisAsc2 [] = True
fisAsc2 [x] = True
fisAsc2 (x:y:xs) =
 (x <= y) && fisAsc2(y:xs)

hasPath :: [(Int,Int)] -> Int -> Int -> Bool
hasPath [] x y = x == y
hasPath xs x y
 | x == y = True
 | otherwise =
  let xs' = [ (n,m) | (n,m) <- xs, n /= x ] in
  or [ hasPath xs' m y | (n,m) <- xs, n == x ]

main :: IO ()
main = do
 let result = hasPath [(1, 2), (2, 3), (3, 2), (4, 3), (4, 5)] 1 2
 print result
