-- Higher order functions
app :: (a -> b) -> a -> b
app f x = f x

add :: Int -> Int
add1 x = x + 1

add add1 1

-- Anonimus function
(\x -> x+1)
add1 = (\x -> x + 1)

(\x y z -> x + y + z)

-- Higher order + Anonimus
app :: (a -> b) -> a ->b
app f x = f x

app (\x -> x + 1) 1 -- => 2

-- map
map (\x -> x + 1) [1, 2, 3, 4, 5] -- => [2, 3, 4, 5, 6]

map (\(x, y) -> x + y) [(1, 2), (2, 3), (3, 4)] -- => [3, 5, 7]

filter (\x -> x > 2) [1, 2, 3, 4, 5] -- => [3, 4, 5]

filter (\(x,y) -> x /= y) [(1, 2), (2, 2)] -- => [(1,2)]
