-- Reverse the list
frev :: [a] -> [a]
-- frev = foldl (\acc x -> x : acc) []
frev = foldl (flip (:)) []

fprefixes :: [a] -> [[a]]
fprefixes = foldr (\x acc -> [x] : (map ((:) x) acc)) []

-- Not done
