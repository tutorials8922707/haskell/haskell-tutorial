-- data Person = Person String Int

data Person = Person {
 name :: String,
 age :: Int
}

-- name :: Person -> String
-- age :: Person -> Int

greet :: Person -> [Char]
-- greet person = "Hi " ++ name person
-- greet (Person name _) = "Hi " ++ name
greet (Person n _) = "Hi " ++ n

data Point = 
 D2 { x :: Int, y :: Int }
 | D3 { x :: Int, y :: Int, z :: Int }
