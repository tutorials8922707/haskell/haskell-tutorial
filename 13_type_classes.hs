-- (+) :: Num a => a -> a -> a

sum' :: Num p => [p] -> p
sum' [] = 0
sum' (x:xs) = x + sum xs

data Temperature = C Float | F Float

instance Eq Temperature where
 (==) (C n) (C m) = n == m
 (==) (F n) (F m) = n == m
 (==) (C c) (F f) = (1.8 * c + 32) == f
 (==) (F f) (C c) = (1.8 * c + 32) == f

data Temperature2 = C2 Float | F2 Float
 deriving (Show, Eq)
