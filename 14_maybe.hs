import Data.Maybe
-- data Maybe a = Nothing | Just a

safediv :: Integral a => a -> a-> Maybe a
safediv a b =
 if b == 0 then Nothing else Just $ div a b

-- safediv 10 0 => Nothing

-- isJust :: Maybe a -> Bool
-- isNothing :: Maybe a -> Bool
-- fromJust :: Maybe a -> a
