-- f :: ?
-- f = <expr>

add x y z = (x + y) : z

-- x :: a
-- y :: b
-- z :: c

-- (+) :: (Num d) => d -> d -> d
-- (:) :: e -> [e] -> [e]

-- from (x + y) derive a = d and b = d
-- from (x + y) : z derive [e] = c and d = e

-- x :: d
-- y :: d
-- z :: [e]
-- z :: [d]

-- add :: (Num d) => d -> d -> [d] -> [d]

-- f = reverse . sort
-- reverse :: [a] -> [a]
-- (.) :: (c -> d) -> (b -> c) -> b -> d
-- sort :: Ord e => [e] -> [e]
