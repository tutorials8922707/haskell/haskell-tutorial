maybeadd :: Num b => Maybe b -> b -> Maybe b
maybeadd mx y = mx >>= (\x -> Just $ x+y)

maybeadd2 :: Num b => Maybe b -> Maybe b -> Maybe b
maybeadd2 mx my = mx >>= (\x -> my >>= (\y -> Just $ x+y))

monadd :: (Monad m, Num b) => m b -> m b -> m b
-- monadd mx my = mx >>= (\x -> my >>= (\y -> return $ x+y))

monadd mx my = do
 x <- mx
 y <- my
 return $ x+y
