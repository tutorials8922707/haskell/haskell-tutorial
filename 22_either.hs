-- data SomeData = Left Int | Right String
data Either a b = Left a | Right b
type SomeData = Prelude.Either Int String
