-- import Module
-- import Module (name1, name2)
-- import Module hiding (name1, name2)
-- import qualified Module -- Module.name
-- import Module as NewName

data DataType = A | B | C
-- import Module (DataType(..))
-- import Module (DataType(A))
-- import Module (DataType(A,C))

-- module Foo
-- (name1
-- ,name2
-- ) where
