-- getArgs :: IO [String]
-- getProgName :: IO String
-- getEvironment :: IO [(String, String)]
-- lookupEnv :: String -> IO (Maybe String)

-- withArgs :: [String] -> IO a -> IO a
-- withProgName :: String -> IO a -> IO a

-- data ExitCode = ExitSuccess | ExitFailure Int
-- exitWith :: Exitcode -> IO a

-- exitFailure :: IO a
-- exitSuccess :: IO a
-- die :: String -> IO a

import Data.Maybe
import System.Environment
import System.Exit

printHelp = do
    progName <- getProgName
    putStrLn $ "Usage: " ++ progName ++ " [-h | --help | -v | --version] <greeting>"

printVersion = putStrLn "v1"

mainAct [] = do
    putStrLn "Needs a greeting!"
    printHelp
    exitFailure
mainAct args = do
    let greeting = unwords args
    name <- lookupEnv "USER"
    putStrLn $ maybe "No user to greet!" (\name -> greeting ++ " " ++ name) name

main = do
    args <- getArgs
    if "-h" `elem` args || "--help" `elem` args
        then printHelp >> exitSuccess
        else
            if "-v" `elem` args || "--version" `elem` args
                then printVersion >> exitSuccess
                else mainAct args
