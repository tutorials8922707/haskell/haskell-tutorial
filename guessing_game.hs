runGame :: Int -> IO ()
runGame incorrctGuesses = do
 let secretNumber = "5"

 if incorrctGuesses == 3
 then putStrLn "Sorry, you lose :("
 else do
  putStrLn "Enter a guess between 1 and 10: "
  userGuess <- getLine
  if userGuess == secretNumber
  then putStrLn "Yay, you win!"
  else runGame (incorrctGuesses + 1)

main :: IO ()
main = do
 runGame 0
